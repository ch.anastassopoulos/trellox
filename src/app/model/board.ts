import { Step } from "./step";
import { User } from "./user";

export interface Board {
    id: number;
    title: string;
    members: User[];
    steps: Step[];
  }