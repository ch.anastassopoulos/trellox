import { CardChecklist } from './cardChecklist';
import { CardComment } from './cardComment';
import { User } from './user';

export interface Card {
    id: number;
    title: string;
    description?: string;
    creationDate?: Date;
    dueDate?: Date;
    color?: string;
    owner?: User;
    stepTitle?: string;
    cardComments: CardComment[];
    cardChecklist?: CardChecklist;
  }