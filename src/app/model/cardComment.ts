import { User } from "./user";

export interface CardComment {
    id: number;
    content: string;
    creationDate: Date;
    owner: User;
  }