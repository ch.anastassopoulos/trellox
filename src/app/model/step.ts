import { Card } from "./card";

export interface Step {
    id: number;
    title: string;
    cards: Card[];
  }