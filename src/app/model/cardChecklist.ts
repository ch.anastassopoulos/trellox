export interface CardChecklistItem {
    id: number;
    content: string;
    checked: boolean;
}

export interface CardChecklist {
    id: number;
    name: string;
    items: CardChecklistItem[];
}