import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { Card } from '../model/card';
import { BOARDS } from './mock-boards';
import { CardComment } from '../model/cardComment';
import { CardChecklist, CardChecklistItem } from '../model/cardChecklist';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  selectedCardSubject!: Subject<number>;

  constructor() {
    this.selectedCardSubject = new Subject<number>();
  }

  subscribeSelectedCard(): Observable<number> {
    return this.selectedCardSubject.asObservable();
  }

  selectCard(id: number): void {
    this.selectedCardSubject.next(id);
  }

  getCard(id: number): Observable<Card> {
    for (let b of BOARDS)
      for (let s of b.steps)
        for (let c of s.cards)
          if (c.id === id) {
            c.stepTitle = s.title;
            return of(c);
          }
    return of(); // TODO replace by DB access
  }

  saveCard(card: Card) {
    // TODO replace by DB access
  }

  saveCardComment(cardComment: CardComment) {
    // TODO replace by DB access
  }

  saveCardChecklist(cardChecklist: CardChecklist) {
    // TODO replace by DB access
  }

  addCardChecklistItem(cardChecklist: CardChecklist, item: string) {
    let id = cardChecklist.items.length == 0 ? 1 : cardChecklist.items.map(i => i.id).reduce((a, b) => a > b ? a : b) + 1;
    cardChecklist.items.push({ id: id, content : item, checked: false });
    // TODO replace by DB access
  }

  removeCardChecklistItem(cardChecklist: CardChecklist, item: CardChecklistItem) {
    cardChecklist.items = cardChecklist.items.filter(i => i.id != item.id);
    // TODO replace by DB access
  }
}
