import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BOARDS } from './mock-boards';
import { Step } from '../model/step';

@Injectable({
  providedIn: 'root'
})
export class StepService {

  constructor() { }

  getStep(id: number): Observable<Step> {
    for (let b of BOARDS)
      for (let s of b.steps)
        if (s.id === id)
          return of(s);
    return of(); // TODO replace by DB access
  }
}
