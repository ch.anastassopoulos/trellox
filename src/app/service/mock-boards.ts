import { Board } from '../model/board';

export var BOARDS: Board[] = [
  {
    id: 1,
    title: 'Gestion projet 1',
    members: [{ id: 1, name: 'Toto', login: 'toto@site.org' }],
    steps: [
      {
        id: 1,
        title: 'Todo',
        cards: [
          {
            id: 6,
            title: 'Création page Product',
            description: 'Créer la page qui affiche les produits',
            cardComments: [],
          },
          {
            id: 7,
            title: 'Création API Command',
            description: "Créer l'API qui fourni les commandes",
            cardComments: [],
          },
          {
            id: 8,
            title: 'Création page Command',
            cardComments: [],
          },
        ],
      },
      {
        id: 2,
        title: 'In progress',
        cards: [
          {
            id: 3,
            title: 'Création API User',
            description: "Créer l'API qui fourni les utilisateurs",
            cardComments: [],
          },
          {
            id: 4,
            title: 'Création page User',
            description: 'Créer la page qui affiche les utilisateurs',
            cardComments: [],
          },
          {
            id: 5,
            title: 'Création API Product',
            description: "Créer l'API qui fourni les produits",
            cardComments: [],
          },
        ],
      },
      {
        id: 3,
        title: 'Done',
        cards: [
          {
            id: 1,
            title: 'Conception générale',
            description: "Concevoir l'architecture générale",
            creationDate: new Date('2024-10-04'),
            dueDate: new Date('2024-10-16'),
            color: '#f06ea9',
            owner: { id: 1, name: 'Toto', login: 'toto@site.org' },
            cardComments: [
              {
                id: 1,
                content: "on va faire ça et ça",
                creationDate: new Date('2024-05-09T09:00:00'),
                owner: { id: 1, name: 'Toto', login: 'toto@site.org' },
              },
              {
                id: 2,
                content: "puis pas oublier...",
                creationDate: new Date('2024-05-09T09:10:00'),
                owner: { id: 1, name: 'Toto', login: 'toto@site.org' },
              }
            ],
            cardChecklist: {
              id: 1,
              name: 'list de courses',
              items: [
                {
                  id: 1,
                  content: "fruit & légume",
                  checked: true
                },
                {
                  id: 2,
                  content: "pain",
                  checked: false
                },
                {
                  id: 3,
                  content: "fromage frais",
                  checked: false
                }
              ]
            }
          },
          {
            id: 2,
            title: 'Création BDD',
            description: 'Créer le schéma, tables et users',
            cardComments: [],
          },
        ],
      },
    ],
  },
];
