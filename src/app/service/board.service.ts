import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BOARDS } from './mock-boards';
import { Board } from '../model/board';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor() { }

  getBoards(): Observable<Board[]> {
    return of(BOARDS);
  }

  getBoard(id: number): Observable<Board> {
    return of(BOARDS.find((b) => b.id === id)!);
  }

}
