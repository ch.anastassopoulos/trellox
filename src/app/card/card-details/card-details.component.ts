import { Component, Input } from '@angular/core';
import { Card } from '../../model/card';
import { CommonModule } from '@angular/common';
import { CardService } from '../../service/card.service';
import { CardCommentComponent } from './card-comment/card-comment.component';
import { CardComment } from '../../model/cardComment';
import { CardChecklistComponent } from './card-checklist/card-checklist.component';

@Component({
  selector: 'app-card-details',
  standalone: true,
  imports: [CommonModule, CardCommentComponent, CardChecklistComponent],
  templateUrl: './card-details.component.html',
  styleUrl: './card-details.component.css',
})
export class CardDetailsComponent {
  @Input({ required: false }) cardId!: number;
  card?: Card;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.getCard();
  }

  getCard() {
    this.cardService.getCard(this.cardId!).subscribe((card) => (this.card = card));
  }

  close(): void {
    this.cardService.selectCard(null!);
  }

  dueDateExpired(): boolean {
    return this.card !== undefined && this.card.dueDate !== undefined && this.card.dueDate < new Date();
  }

  setCardTitle(event: any) {
    this.card!.title = event.srcElement.firstChild.textContent;
    this.cardService.saveCard(this.card!);
  }

  setCardOwner(event: any) {
    this.card!.owner!.name = event.srcElement.firstChild.textContent;
    this.cardService.saveCard(this.card!);
  }

  setCardDueDate(event: any) {
    this.card!.dueDate = event.srcElement.firstChild.textContent;
    this.cardService.saveCard(this.card!);
  }

  onCommentDeleted(cardComment: CardComment) {
    this.card!.cardComments = this.card!.cardComments.filter((c) => cardComment.id !== c.id);
    this.cardService.saveCard(this.card!);
  }
}
