import { Component, Input } from '@angular/core';
import { CardChecklist, CardChecklistItem } from '../../../model/cardChecklist';
import { CommonModule } from '@angular/common';
import { CardService } from '../../../service/card.service';
import { ItemCreationComponent } from './item-creation/item-creation.component';

@Component({
  selector: 'app-card-checklist',
  standalone: true,
  imports: [CommonModule, ItemCreationComponent],
  templateUrl: './card-checklist.component.html',
  styleUrl: './card-checklist.component.css'
})
export class CardChecklistComponent {
  @Input({ required: true }) cardChecklist!: CardChecklist;

  constructor(private cardService: CardService) {}
  
  check(id: number): void {
    this.cardChecklist.items.filter((item) => id === item.id).forEach((item) => item.checked = !item.checked);
    this.cardService.saveCardChecklist(this.cardChecklist);
  }

  onItemAdded(content: string): void {
    this.cardService.addCardChecklistItem(this.cardChecklist, content);
  }

  delete(item: CardChecklistItem): void {
    this.cardService.removeCardChecklistItem(this.cardChecklist, item);
  }

  onItemMouseover(element: any): void {
    let deleteEl = element.querySelector('[name=deleteEl]');
    deleteEl.style.display = 'inline-block';
  }

  onItemMouseout(element: any): void {
    let deleteEl = element.querySelector('[name=deleteEl]');
    deleteEl.style.display = 'none';
  }
}
