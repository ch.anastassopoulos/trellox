import { CommonModule } from '@angular/common';
import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-item-creation',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-creation.component.html',
  styleUrl: './item-creation.component.css'
})
export class ItemCreationComponent {
  @ViewChild('content') content!: ElementRef;
  @Output() added = new EventEmitter<string>();
  editorOpened: boolean = false;

  openEditor(): void {
    this.editorOpened = true;
    this.content.nativeElement.className = "item-creation__content";
    this.content.nativeElement.focus();
  }

  saveNewItem(event: any): void {
    this.editorOpened = false;
    this.content.nativeElement.className = "hidden";
    if (this.content.nativeElement.value !== '') {
      this.added.emit(this.content.nativeElement.value);
      this.content.nativeElement.value = '';
    }
  }
}
