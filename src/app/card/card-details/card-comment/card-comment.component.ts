import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CardComment } from '../../../model/cardComment';
import { CommonModule } from '@angular/common';
import { CardService } from '../../../service/card.service';

@Component({
  selector: 'app-card-comment',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card-comment.component.html',
  styleUrl: './card-comment.component.css'
})
export class CardCommentComponent {
  @Input({ required: true }) cardComment!: CardComment;
  @Output() deleted = new EventEmitter();
  @ViewChild('content') el!: ElementRef;

  constructor(private cardService: CardService) {}

  ngAfterViewInit(): void {
    this.el.nativeElement.innerHTML = this.cardComment!.content;
  }

  delete(): void {
    this.deleted.emit();
  }

  setCardComment(event: any): void {
    this.cardComment!.content = this.el.nativeElement.innerHTML;
    this.cardService.saveCardComment(this.cardComment);
  }
}
