import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Card } from '../model/card';
import { CardService } from '../service/card.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card.component.html',
  styleUrl: './card.component.css',
})
export class CardComponent {
  @Input({ required: true }) cardId!: number;
  card?: Card;
  
  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.getCard();
  }

  getCard() {
    this.cardService.getCard(this.cardId!).subscribe((card) => (this.card = card));
  }

  dueDateExpired(): boolean {
    return this.card !== undefined && this.card.dueDate !== undefined && this.card.dueDate < new Date();
  }

  edit(): void {
    this.cardService.selectCard(this.cardId!);
  }
}
