import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CardDetailsComponent } from './card/card-details/card-details.component';
import { CommonModule } from '@angular/common';
import { CardService } from './service/card.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CardDetailsComponent, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  cardId?: number;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.cardService.subscribeSelectedCard().subscribe((cardId) => this.cardId = cardId);
  }
}
