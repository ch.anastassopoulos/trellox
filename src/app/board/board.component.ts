import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StepComponent } from '../step/step.component';
import { Board } from '../model/board';
import { BoardService } from '../service/board.service';

@Component({
  selector: 'app-board',
  standalone: true,
  imports: [CommonModule, StepComponent],
  templateUrl: './board.component.html',
  styleUrl: './board.component.css',
})
export class BoardComponent {
  board?: Board;

  constructor(
    private route: ActivatedRoute,
    private boardService: BoardService
  ) {}

  ngOnInit(): void {
    this.getBoard();
  }

  getBoard() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.boardService.getBoard(id).subscribe((board) => (this.board = board));
  }
}
