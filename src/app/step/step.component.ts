import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from '../card/card.component';
import { Step } from '../model/step';
import { StepService } from '../service/step.service';

@Component({
  selector: 'app-step',
  standalone: true,
  imports: [CommonModule, CardComponent],
  templateUrl: './step.component.html',
  styleUrl: './step.component.css',
})
export class StepComponent {
  @Input({ required: true }) stepId!: number;
  step?: Step;

  constructor(private stepService: StepService) {}

  ngOnInit(): void {
    this.getStep();
  }

  getStep() {
    this.stepService.getStep(this.stepId!).subscribe((step) => (this.step = step));
  }
}